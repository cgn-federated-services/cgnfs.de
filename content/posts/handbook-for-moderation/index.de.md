---
title: "Handbuch für Moderation"
date: 2023-10-29T22:00:00+01:00
draft: false
author: Malte
---

***Draft***

Dieses Dokument dient als Handbook für neue Moderatior:innen. Es enthält erste Schritte, die bei der Moderation wichtig sind.

## Kategorisierung der Meldung

Jede Meldung muss Kategorisiert werden. Es gibt vier übergeordnete Kategorien, dazu zählen Andere, Rechtlich, Spam und Serverregeln. Nutzer von anderen Servern können grundsätzlich nur die ersten drei auswählen. Das bedeuted als Moderator muss ich die entsprechende Serverregel auswählen, gegen die, der Post verstößt.

Die richtige Regel oder Kategorie auszuwählen ist wichtig, denn die User:in erhält eine Email mit allen Details zur Meldung.

![Kategorie der Meldung](meldung-kategorie.png)

## Maßnahmen bei einer Meldung

Folgend sind alle möglichen Maßnahmen erklärt, die bei jeder Meldung zur verfügung stehen.

- **Als geklärt Markieren:** Schließt die Meldung ohne Aktion, wird nicht als Maßnahme im Profil gezählt.
- **Mit einer Inhaltswarnung versehen:** Die Gemeldeten Medien werden mit einer Inhaltswarnung versehen (CW). Es wird als Maßnahme im Profil gezählt.
- **Beiträge löschen:** Die Beiträge werden unwiederbringlich gelöscht. Es wird als Maßnahme im Profil gezählt.
- **Stummschalten:** Das Konto kann nur über explizite Suche gefunden werden. Beiträge erscheinen nichtmehr in öffentlichen Timelines (Local und Federated). Wichtig: Die Stummschaltung gilt nur auf unserem Server, bei anderen Servern, kann das Profil normal gefunden werden.
- **Sperren:** Das Konto und alle Inhalte von diesem werden gesperrt.
- **Angepasst:** Hier können besonders bei lokalen Accounts weitere Aktionenausgeführt werden. Diese Option wird bei Verwarnungen verwendet. 

![Aktionen als Liste](meldung-aktionen.png)

## Aktuelle Regeln

Auf unserer Instanz gelten aktuell die folgenden Regeln:

1. **Behandle andere respektvoll:** Unterlasse jegliche Formen von Beleidigungen, Mobbing oder Diskriminierung, einschließlich aber nicht ausschließlich rassistischer, sexistischer, homophober, transphober, antisemitischer und frauenfeindlicher Äußerungen oder Handlungen gegenüber anderen Personen.
2. **Verwende einen angemessenen Tonfall:** Verwende einen respektvollen und freundlichen Ton, auch wenn du mit jemandem uneinig bist.
3. **Respektiere die Privatsphäre anderer:** Teile keine persönlichen Informationen ohne ausdrückliche Genehmigung und achte die Datenschutzrechte anderer.
4. **Halte dich an geltendes Recht:** Veröffentliche keine illegalen Inhalte.
5. **Vermeide Falschinformationen:** Teile keine falschen oder irreführenden Informationen.
6. **Keine Spam-Posts:** Verzichte auf das Veröffentlichen unerwünschter Werbung oder ähnlicher Inhalte, die nicht zur Community passen.
7. **Kennzeichnung sensibler Inhalte (CW):** Kennzeichne alle Inhalte, die allgemeine Auslöser enthalten, sexuell explizit oder gewalttätig sind, als sensibel (CW), um die Sicherheit der anderen Mitglieder:innen zu gewährleisten. Hinweis: Konten, die ausschließlich solche Inhalte veröffentlichen, können eingeschränkt oder gesperrt werden.
8. **Du musst älter als 16 Jahre sein:** Nach europäischem Recht musst du mindestens 16 Jahre alt sein, um diese Instanz zu nutzen.

Im folgenden werden zu den einzelnen Regeln erste Moderationsschritte erklärt.

#### 1. Behandle andere respektvoll

Bei dieser Regel ist vermutlich am meisten abwägung von nöten. Die Moderations Aktion kann von `Verwarnung` bis `Beiträge löschen` reichen

#### 2. Verwende einen angemessenen Tonfall

In den meisten Fällen sollte hier nur eine `Verwarnung` erfolgen. Einfach die Vorlage `Use appropriate tone` verwenden.

#### 3. Respektiere die Privatsphäre anderer

Bei privatphäre Verstößen müssen die Beiträge unverzüglich gelöscht werden. Bei Wiederholung wird das Konto gesperrt.

#### 4. Halte dich an geltendes Recht

Illegale Inhalte werden entfernt. Bei mehrfacher Wiederholung wird das Konto gesperrt.

#### 5. Vermeide Falschinformationen

Betroffene Posts werden gelöscht. Bei mehrfacher Wiederholung wird das Konto gesperrt.

#### 6. Keine Spam-Posts

Klassische Spam Accounts lassen sich einfach erkennen. Häufige Merkmale sind:

- Posts nur in fremder Sprache also nicht Englisch oder Deutsch
- Neue Accounts mit keinem anderen Inhalt

Diese Accounts können ohne Verwarnung direkt gesperrt werden. Sollte versehentlich ein falscher Account gesperrt werden, besteht immernoch die Möglichkeit einer Beschwerde.

#### 7. Kennzeichnung sensibler Inhalte (CW)

Wir sperrent Konten, die überwiegend NSFW Inhalte posten. Das kann ohne vorherige Verwarnung geschehen. Bei Posts mit fehlendem CW sollte die Moderations Aktion `Mit einer Inhaltswarnung versehen` verwendet werden.

#### 8. Du musst älter als 16 Jahre sein

Unter 16 Jährige werden gesperrt.

## Beispiel Meldung eines Spam-Accounts

![Meldung eines Spam-Accounts](example-meldung.png)