---
title: "Support via Open Collective"
description: ""
date: 2023-01-28T22:00:00+01:00
draft: false
author: Malte
---

As of about a week ago, our [Open Collective](https://opencollective.com/cgn-federated-services) has been approved. There you can support us with the required funds for the infrastructure.

## What does Open Collective do?

Our Fiscal Host [Open Collective Europe](https://opencollective.com/europe) manages our funds on Open Collective. We can then  submit and approve all expenses made for CGN Federated Services. This improves transparency to a point and also removes the burden of handling the money in our private bank accounts. At the same time it will allow you to pay via SEPA direct debit, credit card or Paypal. If you want to know more about how it works, I recommend to scroll through the [start page](https://opencollective.com/).

## What happens to Ko-fi?

Everyone who supports via Ko-fi should consider switching to Open Collective. I will shut it down in a timely manner.