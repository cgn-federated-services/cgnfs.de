---
title: "Support via Open Collective"
date: 2023-01-28T22:00:00+01:00
draft: false
author: Malte
---

Vor etwa einer Woche wurde unser [Open Collective](https://opencollective.com/cgn-federated-services) genehmigt. Dort kannst du uns bei den Infrastrukturkosten unterstützen.

## Was macht Open Collective?

Unser Fiscal Host [Open Collective Europe](https://opencollective.com/europe) verwaltet unsere finanziellen Mittel auf Open Collective. Wir können dort alle Ausgaben, die für CGN Federated Services getätigt werden, einreichen und genehmigen. Dies verbessert die Transparenz bis zu einem gewissen Grad und wir müssen das Geld nicht auf unseren privaten Bankkonten verwalten. Gleichzeitig ermöglicht es dir, per SEPA-Lastschrift, Kreditkarte oder Paypal zu bezahlen. Wenn du mehr darüber wissen willst, empfehle ich dir einfach durch die [Startseite](https://opencollective.com/) zu scrollen.

## Was passiert mit Ko-fi?

Alle, die über Ko-fi unterstützen sollten sich überlegen, zu Open Collective zu wechseln. Ich werde es zeitnah abzuschalten.